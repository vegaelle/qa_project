from django.db import models
from django.db.models import Q
from django.contrib.auth import get_user_model


User = get_user_model()


class QuestionManager(models.Manager):
    def get_answered_questions(self, qs=None):
        qs = qs or self.all()
        return (
            qs.filter(answer__isnull=False)
            .filter(answer__author__username__istartswith="a")
            .order_by("-answer__publish_date")
            .select_related("answer", "answer__author")
        )

    def search(self, search, qs=None):
        qs = qs or self.all()
        return qs.filter(
            Q(title__icontains=search)
            | Q(text__icontains=search)
            | Q(answer__text__icontains=search)
        )


class Question(models.Model):
    title = models.CharField("titre", max_length=100)
    text = models.TextField("texte")
    publish_date = models.DateTimeField("date de publication", auto_now_add=True)
    image = models.ImageField(
        verbose_name="image", upload_to="question_image", blank=True, null=True
    )

    objects = QuestionManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "question"


class Answer(models.Model):
    """
    Modèle « réponse », attaché à une question. Une réponse peut être postée par un admin

    Examples:

        >>> from qa_app.models import Question, Answer
        >>> from django.contrib.auth import get_user_model
        >>> User = get_user_model()
        >>> admin = User(username='admin')
        >>> admin.save()
        >>> q = Question(title='Première question', text='Texte de la question')
        >>> q.save()
        >>> q.id
        1
        >>> a = Answer(question=q)
        >>> a.text = 'Texte de la réponse'
        >>> a.author = User.objects.first()
        >>> a.save()
        >>> a.id
        1
    """

    question = models.OneToOneField(
        Question, on_delete=models.CASCADE, verbose_name="question"
    )
    text = models.TextField("texte")
    author = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, verbose_name="auteur"
    )
    publish_date = models.DateTimeField("date de publication", auto_now_add=True)

    class Meta:
        verbose_name = "réponse"
