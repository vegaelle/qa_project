from django.contrib import admin

from . import models


class AnswerFilter(admin.SimpleListFilter):

    title = "réponse"
    parameter_name = "has_answer"

    def queryset(self, request, queryset):
        if self.value() == "yes":
            queryset = queryset.filter(answer__isnull=False)
        elif self.value() == "no":
            queryset = queryset.filter(answer__isnull=True)
        return queryset

    def lookups(self, request, model_admin):
        return (
            ("yes", "Oui"),
            ("no", "Non"),
        )


class AnswerInline(admin.TabularInline):

    model = models.Answer
    fields = ["text"]


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):

    list_display = ["title", "publish_date", "has_answer", "answered_by", "image"]
    ordering = ["-publish_date"]
    search_fields = ["title", "text", "answer__text", "answer__author__username"]
    list_per_page = 10
    list_filter = (AnswerFilter, "publish_date", "answer__author")
    inlines = [AnswerInline]
    date_hierarchy = "publish_date"

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related("answer", "answer__author")
        return qs

    def has_answer(self, obj):
        try:
            obj.answer
            return True
        except models.Question.answer.RelatedObjectDoesNotExist:
            return False

    has_answer.short_description = "a une réponse"
    has_answer.boolean = True
    has_answer.admin_order_field = "answer"

    def answered_by(self, obj):
        try:
            return obj.answer.author.username
        except (
            AttributeError,
            models.Question.answer.RelatedObjectDoesNotExist,
            models.Answer.author.RelatedObjectDoesNotExist,
        ):
            return ""

    answered_by.short_description = "réponse de"
    answered_by.admin_order_field = "answer__author__username"

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.author = request.user
            instance.save()
        formset.save_m2m()


# admin.site.register(models.Question, QuestionAdmin)
