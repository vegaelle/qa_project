from django import forms

from .models import Question


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ["title", "text", "image"]


class ContactForm(forms.Form):

    name = forms.CharField(label="votre nom", max_length=100)
    mail = forms.EmailField(label="votre adresse mail")
    subject = forms.CharField(label="sujet de votre message", max_length=200)
    message = forms.CharField(label="votre message", widget=forms.Textarea)
