from textwrap import dedent

from django.shortcuts import render, redirect, reverse
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from django.conf import settings
from django.contrib import messages

# from django.views import generic
# from django.views.generic.edit import ModelFormMixin, ProcessFormView
from django.core.mail import send_mail
from django.contrib.auth import get_user_model

from .models import Question
from .forms import QuestionForm, ContactForm
from . import generics


User = get_user_model()


def question_list(request):
    questions = Question.objects.get_answered_questions()
    search = request.GET.get("search", "")
    if search:
        questions = Question.objects.search(search, qs=questions)
    pages = Paginator(questions, settings.QUESTIONS_PER_PAGE)
    page_number = request.GET.get("page", "1")
    page = pages.get_page(page_number)
    if request.method == "POST":
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request,
                "Votre question a été posée, et sera publiée une fois qu’une réponse y sera apportée.",
            )
            return redirect(reverse("index"))
    else:
        form = QuestionForm()
    return render(
        request,
        "qa_app/question_list.html",
        {"question_list": page, "form": form, "search": search},
    )


class QuestionList(generics.ListAndCreateView, generics.SearchMixin):

    queryset = Question.objects.get_answered_questions()
    paginate_by = settings.QUESTIONS_PER_PAGE
    fields = ["title", "text", "image"]
    model = Question
    success_url = reverse_lazy("index")
    success_message = "Votre question a été posée, et sera publiée une fois qu’une réponse y sera apportée."


def contact(request):
    # breakpoint()  # depuis python3.7

    # import pdb
    # pdb.set_trace()

    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            admins = User.objects.filter(is_superuser=True).exclude(email="")
            if admins.count() == 0:
                form.add_error(
                    None,
                    "Le site n'est pas en mesure d'envoyer un message. Réessayez plus tard.",
                )
            else:
                send_mail(
                    f'Message depuis le site Django : {data["subject"]}',
                    dedent(
                        f"""
                          Bonjour.
                          
                          Vous avez reçu un nouveau message via le formulaire de contact de votre site, par
                          {data['name']} <{data['mail']}> :
                          
                          {data['message']}
                          
                          Cordialement,
                          -- 
                          Votre site
                          """
                    ),
                    from_email=None,
                    recipient_list=map(lambda u: u.email, admins),
                )
                messages.success(request, "Votre message a été envoyé avec succès.")
                return redirect(reverse("index"))
    else:
        form = ContactForm()
    return render(request, "qa_app/contact.html", {"form": form})
