Changelog
=========

- **2021-02-08**: Add tests and documentation, refactor generic views
- **2021-01-18**: First version