.. QA Project documentation master file, created by
   sphinx-quickstart on Mon Feb  8 15:32:54 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to QA Project's documentation!
======================================

Ceci est la documentation du projet QA Project

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   changelog.rst
   models.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
